.DEFAULT_GOAL := help

TAG = 6.1.0
ELASTIC_PASSWORD = 123456
build_es: ## build image, use me with: make build_es
	TAG=${TAG} ELASTIC_PASSWORD=${ELASTIC_PASSWORD} docker-compose -f docker-compose.elastic.yml build

up_es: ## up docker containers, use me with: make up_es
	#@make verify_network &> /dev/null
	TAG=${TAG} ELASTIC_PASSWORD=${ELASTIC_PASSWORD} docker-compose -f docker-compose.elastic.yml up -d --build
	TAG=${TAG} ELASTIC_PASSWORD=${ELASTIC_PASSWORD} docker-compose -f docker-compose.elastic.yml ps
	TAG=${TAG} ELASTIC_PASSWORD=${ELASTIC_PASSWORD} docker-compose -f docker-compose.elastic.yml run --rm configure_kibana

down_es: ## Stops and removes the docker containers, use me with: make down_es
	docker-compose -p $(PROYECT_NAME) down

verify_network:
	@if [ -z $$(docker network ls | grep $(DOCKER_NETWORK) | awk '{print $$2}') ]; then\
		(docker network create $(DOCKER_NETWORK));\
	fi

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-16s\033[0m %s\n", $$1, $$2}'